package ru.bell.producer.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class NewKafkaTopicConfig {

	@Value("${kafka.topic.toMainEducationalTopic}")
	private String toMainEducationalTopic;

	@Bean
	public NewTopic topic() {
		return TopicBuilder.name(toMainEducationalTopic)
				.partitions(1)
				.replicas(1)
				.build();
	}
}