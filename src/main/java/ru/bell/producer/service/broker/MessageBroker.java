package ru.bell.producer.service.broker;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import ru.bell.mainlib.dto.EmployeeDto;

import java.util.List;


@Slf4j
@Service
@RequiredArgsConstructor
public class MessageBroker {

	private final KafkaTemplate<String, Object> kafkaTemplate;
	@Value("${kafka.topic.toMainEducationalTopic}")
	private String toMainEducationalTopic;

	public void sendEmployee(List<EmployeeDto> employee) {
		log.info(String.format("#### -> Producing message -> %s", employee));
		kafkaTemplate.send(toMainEducationalTopic, employee);
	}
}