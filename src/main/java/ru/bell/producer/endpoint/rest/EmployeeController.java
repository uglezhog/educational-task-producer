package ru.bell.producer.endpoint.rest;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.bell.mainlib.dto.EmployeeDto;
import ru.bell.producer.service.broker.MessageBroker;

import java.util.List;

@RestController
@AllArgsConstructor
class EmployeeController {

	private final MessageBroker producer;

	@PostMapping("/employees")
	void newEmployee(@RequestBody List<EmployeeDto> newEmployee) {
		producer.sendEmployee(newEmployee);
	}

}